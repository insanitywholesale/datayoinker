package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func buildUp(t *testing.T) string {
	// Mark as helper
	t.Helper()

	// Create temporary file to use for sqlite
	f, err := os.CreateTemp("", "yoinkdb")
	if err != nil {
		t.Fatal("Error creating temp db file: %w", err)
	}
	// Store filename
	fileName := f.Name()

	t.Setenv("DB_PATH", fileName)

	// Create database connection
	testdb, err := SetupDB()
	if err != nil {
		t.Fatal("Setting up the database failed: %w", err)
	}

	// Set global db variable
	db = testdb

	return fileName
}

func tearDown(t *testing.T, testPath string) {
	// Mark as helper
	t.Helper()

	// Delete temp db file
	err := os.Remove(testPath)
	if err != nil {
		t.Fatal("Cleaning up temporary test file failed: %w", err)
	}
}

// publishYoink is a helper for sending data to testtopic
func publishYoink(t *testing.T, topic string, data string, shouldWork bool) *Yoink {
	// Mark as helper
	t.Helper()

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/publish/yoink/for/"+topic+"?"+data, nil)
	// The URL should be set to some bs if topic is empty and the publish is meant to fail
	if topic == "" && !shouldWork {
		req = httptest.NewRequest(http.MethodGet, "/publish/yoink/for/@#?"+data, nil)
	}

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}
	// Convert response data to string and store it
	got := string(body)

	// Create empty object to be used as a decoding target
	y := &Yoink{}

	// Decode response body into empty object
	dec := json.NewDecoder(strings.NewReader(got))
	dec.DisallowUnknownFields()
	err = dec.Decode(y)
	if !shouldWork {
		assert.Equal(t, y, new(Yoink))
		t.Log("Malformed JSON was correctly rejected")

		return nil
	}

	require.NoError(t, err)
	assert.NotNil(t, y)

	return y
}

// getLastestYoink is a helper for getting the latest yoink from testtopic
func getLatestYoink(t *testing.T, topic string, shouldWork bool) *Yoink {
	// Mark as helper
	t.Helper()

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/get/latest/yoink/from/test", nil)
	// The URL should be set to some bs if topic is empty and the publish is meant to fail
	if topic == "" && !shouldWork {
		req = httptest.NewRequest(http.MethodGet, "/get/latest/yoink/from/@#", nil)
	}

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}
	// Convert response data to string and store it
	got := string(body)

	// Create empty object to be used as a decoding target
	y := &Yoink{}

	// Decode response body into empty object
	dec := json.NewDecoder(strings.NewReader(got))
	dec.DisallowUnknownFields()
	err = dec.Decode(y)
	if !shouldWork {
		assert.Equal(t, y, new(Yoink))
		t.Log("Malformed JSON was correctly rejected")

		return nil
	}

	require.NoError(t, err)
	assert.NotNil(t, y)

	return y
}

// getAllYoinks is a helper for getting all yoinks from testtopic
func getAllYoinks(t *testing.T) []*Yoink {
	// Mark as helper
	t.Helper()

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/get/all/yoinks/from/test", nil)

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}
	// Convert response data to string and store it
	got := string(body)

	// Create list of empty yoinks to be used as a decoding target
	ys := []*Yoink{}

	// Decode response body into empty object
	err = json.NewDecoder(strings.NewReader(got)).Decode(&ys)
	if err != nil {
		t.Errorf("Decoding response failed: %v", err)
	}

	return ys
}

// getLastestNumberOfYoinks is a helper for getting the latest yoink from testtopic
func getLastestNumberOfYoinks(t *testing.T, num string) []*Yoink {
	// Mark as helper
	t.Helper()

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/get/last/"+num+"/yoinks/from/test", nil)

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}
	// Convert response data to string and store it
	got := string(body)

	// Create list of empty yoinks to be used as a decoding target
	ys := []*Yoink{}

	// Decode response body into empty object
	err = json.NewDecoder(strings.NewReader(got)).Decode(&ys)
	if err != nil {
		t.Errorf("Decoding response failed: %v", err)
	}

	return ys
}

// TestSetupPort tests the SetupPort function
// It checks that the port has the correct default value and can be correctly changed
func TestSetupPort(t *testing.T) {
	if SetupPort() != "3333" {
		t.Fatal("Default app port value was not the expected one")
	}

	testPort := "53333"
	t.Setenv("DATAYOINKER_PORT", testPort)
	if SetupPort() != testPort {
		t.Fatal("Altered app port value was not the specified one")
	}
}

// TestSetupDB tests that the setupDB function does not error out
// It also checks that an alternate database path can be used without issue
func TestSetupDB(t *testing.T) {
	testPath := "/tmp/yoinker.db"
	t.Setenv("DB_PATH", testPath)

	db, err := SetupDB()
	if err != nil {
		if db != nil {
			t.Fatal("Error was encountered but database isn't nil")
		}
		t.Fatalf("Setting up the database failed: %v", err)
	}

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestQuickstart(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/quickstart", nil)

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}

	t.Log(string(body))

	// Check that the response body wasn't empty
	assert.NotEmpty(t, string(body))

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestGetVersionInfo(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	// Create request
	req := httptest.NewRequest(http.MethodGet, "/info", nil)

	// Create test HTTP recorder
	w := httptest.NewRecorder()

	// Create HTTP router and send request
	setupRouter().ServeHTTP(w, req)

	// Store test HTTP recorder response
	res := w.Result()
	defer res.Body.Close()

	// Read all response body data
	body, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Reading response body failed: %v", err)
	}

	t.Log(string(body))

	// Check that the response body wasn't empty
	assert.NotEmpty(t, string(body))

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestPublishForTopic(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	// Use helper to send test data into a test topic
	y := publishYoink(t, "test", "num=666.666&threads=7&result=discard&zooted=false", true)

	// Test that there was no other yoink in the test database
	if y.ID != 1 {
		t.Fatalf("case 1: wrong yoink returned: %#v", y)
	}
	// Test that numbers are handled correctly
	if y.Content["num"] != 666.666 {
		t.Fatalf("case 2: wrong yoink returned: %#v", y)
	}
	// Test that ints which in JSON are floats too are handled correctly
	if y.Content["threads"] != float64(7) {
		t.Fatalf("case 3: wrong yoink returned: %#v", y)
	}
	// Test that strings are handled correctly
	if y.Content["result"] != "discard" {
		t.Fatalf("case 4: wrong yoink returned: %#v", y)
	}

	// Use helper to send incorrect test data into the test topic
	assert.Nil(t, publishYoink(t, "test", "result=bad&num=666.666&threads=7&result=discard&zooted=false", false))

	// Use helper to send incorrect test data into the test topic
	assert.Nil(t, publishYoink(t, "test", "result=b}ad&num=666.6\"66&threads=7&&&zooted=false", false))

	// Use helper to send incorrect test data into the test topic
	assert.Nil(t, publishYoink(t, "", "threads=7&&&zooted=false", false))

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestGetLatestYoinkFromTopic(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	// Use helper to send test data into the test topic
	// The API returns the JSON which is then put into an object and returned by the helper
	y1 := publishYoink(t, "test", "num=666.666&threads=7&result=discard", true)

	// Use helper to get latest data from the test topic
	y2 := getLatestYoink(t, "test", true)

	// Check that what we sent and what we got back are the same
	assert.Equal(t, y1, y2)

	// Use helper to make bad request for getting latest data from topic
	assert.Nil(t, getLatestYoink(t, "", false))

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestGetAllYoinksFromTopic(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	y1 := publishYoink(t, "test", "num=666.666&threads=7&result=discard", true)
	y2 := publishYoink(t, "test", "yoinker=zoinker&zooted=false", true)
	assert.NotNil(t, y1)
	assert.NotNil(t, y2)

	testYs := []*Yoink{y1, y2}

	// Get all yoinks from testtopic
	ys := getAllYoinks(t)
	t.Log(ys)

	for i, testY := range testYs {
		for key, value := range ys[i].Content {
			for k, val := range testY.Content {
				if key == k {
					if value != val {
						t.Fatalf("String yoink content mismatch, expected: %v got: %v", val, value)
					}
					t.Log("val: ", val, "value: ", value, "type: ", fmt.Sprintf("%T", value))
				}
			}
		}
	}

	// Clean up temp DB file
	tearDown(t, testPath)
}

func TestGetLastNumberOfYoinksFromTopic(t *testing.T) {
	// Create temp db file and store path
	testPath := buildUp(t)

	// Use helper to send test data into the test topic
	// The API returns the JSON which is then put into an object and returned by the helper
	sentYs := []*Yoink{
		publishYoink(t, "test", "testdataserial=1", true),
		publishYoink(t, "test", "testdataserial=2", true),
		publishYoink(t, "test", "testdataserial=3", true),
		publishYoink(t, "test", "testdataserial=4", true),
		publishYoink(t, "test", "testdataserial=5", true),
	}

	receivedYs := getLastestNumberOfYoinks(t, strconv.Itoa(len(sentYs)))
	assert.ElementsMatch(t, sentYs, receivedYs)

	// Clean up temp DB file
	tearDown(t, testPath)
}
